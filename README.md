# IndabaX-GAS-tools

A set of nifty tools for Google App scripts code to help manage around automatic document generation from forms for IndabaX conferences. For example generating invoices, recipes, contracts etc... 


# Getting started 
## Clasp 
Guide 
- https://developers.google.com/apps-script/guides/clasp
- https://codelabs.developers.google.com/codelabs/clasp/#1

Requires Node and NPM 

### Node and NPM
https://docs.npmjs.com/cli/v7/configuring-npm/install
Linux (recommened to build from source)
```
curl -fsSL https://deb.nodesource.com/setup_19.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
```

### Install clasp 
```
npm install @google/clasp -g
```
Try using `sudo npm install @google/clasp -g` if there is a permissions issue  

### Clone or create an existing GAS project 
e.g. 
```
clasp clone 1xwzpih-GRi1-V7RycYWgL5ew0gEbCVKx2lvczmsbmz0CX0n9Q3wNCBAG 
```

### Push changes 
Enable GAS API 
https://script.google.com/home/usersettings 

may need to 
```
npm init --yes
```
in project root directory if getting error 
`ENOENT: no such file or directory, open 'package.json'` 


# TypeScript 
Why TypeScript - https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html 

How to use TypeScript with GAS
- https://developers.google.com/apps-script/guides/typescript
- https://github.com/google/clasp/blob/master/docs/typescript.md


# Resources 
## Type script, git and clasp 
https://developers.google.com/apps-script/guides/typescript
https://developers.google.com/apps-script/guides/clasp
https://stackoverflow.com/questions/48791868/use-typescript-with-google-apps-script
https://webapps.stackexchange.com/questions/163341/version-control-for-google-apps-script-project

## git GAS 
https://github.com/brucemcpherson/gasGit | https://ramblings.mcpher.com/drive-sdk-and-github/getting-your-apps-scripts-to-github/ 
https://stackoverflow.com/questions/30866467/google-apps-script-and-source-code-tools-for-version-control
https://chrome.google.com/webstore/detail/google-apps-script-github/lfjcgcmkmjjlieihflfhjopckgpelofo

## Old 
Some useful resources... 
https://docs.google.com/spreadsheets/d/10kkgWTFISlsHikIxSH1gkIsbWpVqvLt8eOmOKFhFxPE/htmlview 
https://medium.com/@max.brawer/learn-to-magically-send-emails-from-your-google-form-responses-8bbdfd3a4d02
http://www.tjhouston.com/2012/03/merge-info-from-google-forms-to-pdf-document-and-send-via-email/
https://stackoverflow.com/questions/33486329/short-script-for-converting-google-forms-data-to-pdf-not-sending-email
https://jeffreyeverhart.com/2016/08/06/fix-typeerror-cannot-read-property-values-undefined/
https://developers.google.com/apps-script/guides/triggers/installable
https://developers.google.com/apps-script/guides/triggers/events


# Testing 
Testing pre-filled form link 
https://docs.google.com/forms/d/e/1FAIpQLSfC3j2xe9TvAhZSFr6NtJR-EWVtH2ksGVY8lBuv4nu1U0nLZA/viewform?usp=pp_url&entry.759251887=1234356&entry.1000020=test_company_website_name&entry.376933095=test.com&entry.977203302=https://www.chrisfourie.africa/index.assets/chris-pic.jpg&entry.1037125717=test_logo&entry.622813078=Main+Conference&entry.1805012489=African+Elephant+-+R100k&entry.1565168879=Unconference+(%2B+R5k+%7C+Cape+Buffalo+and+up)&entry.2082671490=such_fellowship_much_wow&entry.699113339=Yes&entry.793144055=Notepad&entry.793144055=USB+driveon+16+of+18+Untitled+section&entry.956304041=test_company_reg+name&entry.1443445283=test_reg+number+1234&entry.1912557604=vat+num+test


