
// Invoice 
var invoice_template_id = "1SobcfjSf4lo0ArAIQwyTX2dA4cq0vHhvXr-B-H4vTZg"; // sheet template for invoice 
var invoice_folder_id = "1JHk8XJRs5JHdH0jAl_LasIllALtO9FTK"; // store invoices in pdf form here
var invoice_sheet_template = SpreadsheetApp.openById(invoice_template_id);

// Sponsor contract 
var doc_template = "1lRqvL62vqp219J-ph0SYmPT16of0E4I_6RiPX1E2KQE"; 
var doc_name = "Deep Learning IndabaX South Africa 2020 Sponsorship";

// Tier mappings 
var tier_amounts = {
  'African Elephant - R100k': 100000,
  'Black Rhino - R50k': 50000,
  'Cape Buffalo - R25k': 25000,
  'Lion - R15k': 15000,
  'Leopard - Hackathon + R10k in prizes': -1,
  'White Rhino - custom': -2,
  'Elephant Shrew - R25k': 100000,
  'Rhino Bettle - R15k': 50000,
  'Buffalo Weaver - R10k': 20000,
  'Ant Lion - R5k': 10000,
}; 

var tier_addon_amounts = {
  "Closing event (+ R10k | African Elephant only)" : 10000,
  "Unconference (+ R5k | Cape Buffalo and up)" : 5000,
  "Poster Session (+ R5k | Cape Buffalo and up)" : 5000, 
}

// When Form Gets submitted we get an event object e 
function onSponsorFormSubmit(e) {
  var invoice_pdf = parseSponsorFormToInvoice(e);
  // var contract_pdf = parseFormToContract(e); // TODO: update code to generate contract - prioritize if multiple sponsors request a contract 

  var email_address = e.namedValues["Email address"][0];
  var name_website = e.namedValues["Company name (as it will be displayed on the website)"][0];
  Logger.log("sending email");
  sendEmail(email_address, name_website, invoice_pdf);
  Logger.log("email sent");
}

function sendEmail(email_address, name_website, invoice_pdf) { // TODO: add contract_pdf arg 
  // Attach PDF and send the email
  var subject = "[DLXSA + " + name_website + "] Sponsorship Invoice";

  var body = "Greetings " + name_website + ",<br><br>Please see attached for invoice "
    + "pertaining to the details submitted on the IndabaX sponsorship form.<br><br>"
    + "If there are any changes that need to be made, please email sponsors@indabax.co.za "
    + "<br><br>Kind regards,"
    + "<br><br>The IndabaX Team =)";

  // Send a copy to the company for immediate feedback from the form (so they know it worked) and one to the sponsorship team 
  GmailApp.sendEmail(email_address, subject, body, {
    htmlBody: body,
    cc: 'sponsors@indabax.co.za',  
    replyTo: 'sponsors@indabax.co.za',
    attachments: [invoice_pdf], // TODO: add contract_pdf 
    from: 'indabax.za@gmail.com' // SETUP STEP: Requires that your Gmail account is granted delegated access to the IndabaX Gmail account // Guide for delegated access https://support.google.com/mail/answer/138350?hl=en#zippy=  // it also requires that AppScript has the correct permissions to be allowed to send Gmail things - to do this, from the AppScript webb IDE, just try run the sendEmail() function, it will fail, however a dialogue / prompt will open to 'allow access' for this function to Gmail things  
  });
}


function parseSponsorFormToInvoice(e) {
  /*Form values  
  The event object e has various attributes - here we are using the namedValues 
  attribute that relates to the columns in the responses spread sheet.

  The '||' (OR statement) is used to replace KEYS in documents with a blank string,
  as null will leave the KEY in place e.g. IF a sponsor does not enter a 'registration number' 
  THEN the string 'KEY_REG_NUMBER' will remain in the document / sheet and 
  does not look great. */
  // 
  var reg_name = e.namedValues["Registered company name"][0]; // Does not not need '||' as this is a required field in the form
  var tier = e.namedValues["Which tier are you selecting?"][0]; 
  var reg_number = e.namedValues["Registration number"][0] || "Not provided";
  var vat_number = e.namedValues["VAT number"][0] || "Not provided";
  var physical_address = e.namedValues["Company physical address"][0];
  var tier_addon = String(e.namedValues["Optional additional add-on for selected tier"]).split(', ') || ""; 
 // The select multiple options "Checkboxes" form question returns a string that needs to be split into an array, delimited by ', '

  // Tier base amount 
  var amount = tier_amounts[tier]; 

  // Hackathon amount 
  // need to get amount from 'What TOTAL contribution will you be providing?' and 'What percentage will be MONETARY?' fields
  if (amount == -1) {
    var custom_contribution = e.namedValues['What TOTAL contribution will you be providing?'];
    // options all start with 'R' and end with 'k'
    var k_idx = custom_contribution.indexOf('k');
    custom_contribution = parseFloat(custom_contribution.substring(1, k_idx));
    var money_perc = parseFloat(e.namedValues['What percentage will be MONETARY?']);
    if (money_perc <= 1 && money_perc > 0) {
      money_perc = money_perc * 100;
    }
    amount = custom_contribution * money_perc
  }

  // White rhino amount 
  // need to get amount from 'Sponsorship amount'
  else if (amount == -2) {
    amount = e.namedValues['Sponsorship amount'];
  }

  // Tier addon amount 
  var tier_addon_amount = 0;
  for (var i = 0; i < tier_addon.length; i++){
    tier_addon_amount = tier_addon_amount + tier_addon_amounts[tier_addon[i]];
    amount = amount + tier_addon_amount; 
  }
  Logger.log(`Amount: ${amount}`) 

  // Get document template - copy it as a new temp doc - save the Doc’s id
  Logger.log("copying template sheet...");
  invoice_sheet_template.setActiveSheet(invoice_sheet_template.getSheetByName('TEMPLATE'));
  var invoice_sheet = invoice_sheet_template.duplicateActiveSheet(); // TODO copy sheet to new file - easier for intuitive record keeping 
  var invoice_code_suffix = Utilities.getUuid().substring(0,4);
  var invoice_code = reg_name.split(" ")[0] + "_" + invoice_code_suffix

  invoice_sheet.setName(invoice_code);
  Logger.log("copied");

  // Get the sheet's range
  var data_range = invoice_sheet.getDataRange();
  Logger.log("replacing keys");  
  replaceInSheet(data_range, 'KEY_TIER', tier);
  replaceInSheet(data_range, 'KEY_AMOUNT', amount);
  replaceInSheet(data_range, 'KEY_REG_NAME', reg_name);
  replaceInSheet(data_range, 'KEY_INVOICE_NUM', invoice_code);
  replaceInSheet(data_range, 'KEY_REG_NUMBER', reg_number);
  replaceInSheet(data_range, 'KEY_VAT_NUMBER', vat_number);
  replaceInSheet(data_range, 'KEY_PHYSICAL_ADDRESS', physical_address);
  replaceInSheet(data_range, 'KEY_NOTES', "");
  Logger.log("replaced");

  Logger.log("convert to pdf");
  // Convert temporary template document to PDF
  var pdf = getPDF(invoice_sheet.getSheetId(), invoice_code);
  Logger.log("converted");
  Logger.log("Successfully created invoice");
  return pdf;
}


function replaceInSheet(data_range, to_replace, replace_with) {
  var data = data_range.getValues();
  var value = "";
  // Scan through entire sheet to replace string 
  for (var row = 0; row < data.length; row++) {
    for (var col = 0; col < data[row].length; col++) {
      value = String(data[row][col]);
      if (value.includes(to_replace)) {
        var new_value = value.replace(to_replace,replace_with)
        data_range.getCell(row+1,col+1).setValue(new_value);
      }
    }
  }
}


function getPDF(id, name) {
  SpreadsheetApp.flush();
  //make pdf
  var theurl = 'https://docs.google.com/spreadsheets/d/'
    + invoice_template_id  //the file ID
    + '/export?exportFormat=pdf&format=pdf'
    + '&size=LETTER'
    + '&portrait=true'
    + '&fitw=true'
    + '&top_margin=0.50'
    + '&bottom_margin=0.50'
    + '&left_margin=0.50'
    + '&right_margin=0.50'
    + '&sheetnames=false&printtitle=false'
    + '&pagenum=false'
    + '&gridlines=false'
    + '&fzr=FALSE'
    + '&gid='
    + id;       //the sheet's Id

  var token = ScriptApp.getOAuthToken();
  name = name.replace('.pdf', '') + '.pdf'; // remove pdf filetype in name as it's added already
  var docurl = UrlFetchApp.fetch(theurl, { headers: { 'Authorization': 'Bearer ' + token } });
  var file_id = DriveApp.createFile(docurl.getBlob()).setName(name).getId();
  var file_to_del = DriveApp.getFileById(file_id);
  var folder = DriveApp.getFolderById(invoice_folder_id)
  var new_file = file_to_del.makeCopy(folder);
  // clean up
  DriveApp.removeFile(file_to_del);

  return new_file
}

// POLYFILL string functions in javascript but not in App Script

String.prototype.padStart = function padStart(targetLength, padString) {
  targetLength = targetLength >> 0; //truncate if number or convert non-number to 0;
  padString = String((typeof padString !== 'undefined' ? padString : ' '));
  if (this.length > targetLength) {
    return String(this);
  }
  else {
    targetLength = targetLength - this.length;
    if (targetLength > padString.length) {
      padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
    }
    return padString.slice(0, targetLength) + String(this);
  }
};

String.prototype.repeat = function (count) {
  'use strict';
  if (this == null) {
    throw new TypeError('can\'t convert ' + this + ' to object');
  }
  var str = '' + this;
  count = +count;
  if (count != count) {
    count = 0;
  }
  if (count < 0) {
    throw new RangeError('repeat count must be non-negative');
  }
  if (count == Infinity) {
    throw new RangeError('repeat count must be less than infinity');
  }
  count = Math.floor(count);
  if (str.length == 0 || count == 0) {
    return '';
  }
  // Ensuring count is a 31-bit integer allows us to heavily optimize the
  // main part. But anyway, most current (August 2014) browsers can't handle
  // strings 1 << 28 chars or longer, so:
  if (str.length * count >= 1 << 28) {
    throw new RangeError('repeat count must not overflow maximum string size');
  }
  var maxCount = str.length * count;
  count = Math.floor(Math.log(count) / Math.log(2));
  while (count) {
    str += str;
    count--;
  }
  str += str.substring(0, maxCount - str.length);
  return str;
}

