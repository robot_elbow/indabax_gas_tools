function runOnFormSubmitTests(e){
    testGetFormNamedValues(e)
    testGetSpecificNamedValues(e)
    testParseFormToInvoice(e)
}

function testGetFormNamedValues(e){
    Logger.log(e.namedValues) 
}

function testGetSpecificNamedValues(e){
    var namedValues = e.namedValues 
    Logger.log(namedValues["Link to logos we can use"])
}

function testParseFormToInvoice(e){
    parseSponsorFormToInvoice(e)
}