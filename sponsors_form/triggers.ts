function createTestingTriggers(){
var sheet = SpreadsheetApp.getActive();
ScriptApp.newTrigger("runOnFormSubmitTests")
  .forSpreadsheet(sheet)
  .onFormSubmit()
  .create();
} 


function createProductionTriggers(){
  var sheet = SpreadsheetApp.getActive();
  ScriptApp.newTrigger("onSponsorFormSubmit")
    .forSpreadsheet(sheet)
    .onFormSubmit()
    .create();
  } 